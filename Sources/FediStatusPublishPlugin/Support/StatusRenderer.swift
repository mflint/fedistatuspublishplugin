import Foundation
import Plot

public protocol StatusRenderer {
	func render(status: Status) throws -> String
}

private struct RawText: Component {
	var body: Component { node }
	private var node: Node<HTML.BodyContext>

	init(_ string: String) {
		self.node = .raw(string)
	}
}

public final class DefaultStatusRenderer: StatusRenderer {
	private static let dateFormatter: DateFormatter = {
		let df = DateFormatter()
		df.dateFormat = "MMM dd yyyy, HH:mm"
		return df
	}()

	public init() {}
	
	public func render(status: Status) throws -> String {
		Div {
			Paragraph {
				Image(url: status.account.avatar, description: "")

				Link(url: status.account.url) {
					Text(status.account.displayName)
						.addLineBreak()
				}

				if let instanceHost = status.account.instanceHost {
					Span {
						Link(url: status.account.url) {
							Text("@\(status.account.username)@\(instanceHost)")
								.addLineBreak()
						}
					}
					.class("fediverseuser")
				}
			}
			.class("fediversestatusheader")

			Paragraph {
				RawText(status.content)

				List {
					Link(url: status.url) {
						Text(Self.dateFormatter.string(from: status.createdAt))
					}

					Link(url: status.reblogsURL) {
						Text(" \(status.reblogsCount)")
					}
					.attribute(named: "aria-label",
							   value: String(format: "%d reblogs", status.reblogsCount))

					Link(url: status.favouritesURL) {
						Text(" \(status.favouritesCount)")
					}
					.attribute(named: "aria-label",
							   value: String(format: "%d favourites", status.favouritesCount))
				}
				.class("fediversemetadata")
			}
		}
		.class("fediversestatus")
		.render()
	}
}
