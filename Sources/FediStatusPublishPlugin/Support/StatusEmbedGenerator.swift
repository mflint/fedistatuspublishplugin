import Foundation

#if canImport(FoundationNetworking)
import FoundationNetworking
#endif

private extension URLSessionConfiguration {
	class func FediStatusSessionConfiguration() -> URLSessionConfiguration {
		let config = URLSessionConfiguration.ephemeral
		config.timeoutIntervalForRequest = 10
		return config
	}
}

final class StatusEmbedGenerator {
	private enum Error: LocalizedError {
		case badStatusURL(urlString: String)
		case timeout(url: URL)
		case unexpectedRepsonse(url: URL)
		case httpErrorStatus(code: Int, url: URL)
		case noData
		case errorDecoding(url: URL, error: Swift.Error)

		var localizedDescription: String {
			switch self {
			case .badStatusURL(let urlString):
				return "Not a valid URL: \(urlString)"
			case .timeout(let url):
				return "The request to fediverse server timed out (\(url.absoluteString))"
			case .unexpectedRepsonse(let url):
				return "Unexpected response from fediverse server (\(url.absoluteString))"
			case .httpErrorStatus(let code, let url):
				return "Unexpected status code \(code) from fediverse server (\(url.absoluteString))"
			case .noData:
				return "Received no data from fediverse server"
			case .errorDecoding(let url, let error):
				return "Error decoding response from fediverse server (\(url.absoluteString)). Error \(error)"
			}
		}
	}

	private static var utcPosixDateParser: DateFormatter = {
		// set timezone and locale
		// see "Working With Fixed Format Date Representations"
		// https://developer.apple.com/documentation/foundation/dateformatter
		let result = DateFormatter();
		result.timeZone = TimeZone(secondsFromGMT: 0)
		result.locale = Locale(identifier: "en_US_POSIX")
		result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
		return result
	}()

	func getStatus(statusUrlString: String) -> Result<Status, Swift.Error> {
		guard let statusUrl = URL(string: statusUrlString),
			  let scheme = statusUrl.scheme,
			  let host = statusUrl.host,
			  let statusId = statusUrl.pathComponents.last,
			  let url = URL(string: "\(scheme)://\(host)/api/v1/statuses/\(statusId)") else {
			return .failure(Error.badStatusURL(urlString: statusUrlString))
		}

		var result: Result<Status, Swift.Error> = .failure(Error.timeout(url: url))
		let sema = DispatchSemaphore(value: 0)

		let session = URLSession(configuration: URLSessionConfiguration.FediStatusSessionConfiguration())

		let task = session.dataTask(with: url) { data, res, error in
			defer { sema.signal() }

			guard let res = res as? HTTPURLResponse else {
				result = .failure(Error.unexpectedRepsonse(url: url))
				return
			}

			guard res.statusCode == 200 else {
				result = .failure(Error.httpErrorStatus(code: res.statusCode, url: url))
				return
			}

			guard let data = data else {
				if let error = error {
					result = .failure(error)
				} else {
					result = .failure(Error.noData)
				}
				return
			}

			do {
				let decoder = JSONDecoder()
				decoder.dateDecodingStrategy = .custom({ decoder in
					let container = try decoder.singleValueContainer()
					let dateString = try container.decode(String.self)
					return Self.utcPosixDateParser.date(from: dateString)!
				})
				let status = try decoder.decode(Status.self, from: data)
				result = .success(status)
			} catch {
				result = .failure(Error.errorDecoding(url: url, error: error))
			}
		}

		task.resume()

		_ = sema.wait(timeout: .now() + 15)

		return result
	}
}
