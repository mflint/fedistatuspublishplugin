import Foundation

public struct Status: Codable {
	public let content: String
	public let createdAt: Date
	public let url: URL
	public let reblogsCount: UInt
	public let favouritesCount: UInt
	public let account: Account

	public var reblogsURL: URL {
		var url = self.url
		url.appendPathComponent("reblogs")
		return url
	}

	public var favouritesURL: URL {
		var url = self.url
		url.appendPathComponent("favourites")
		return url
	}

	public enum CodingKeys: String, CodingKey {
		case content
		case createdAt = "created_at"
		case url
		case reblogsCount = "reblogs_count"
		case favouritesCount = "favourites_count"
		case account
	}
}

public struct Account: Codable {
	public let username: String
	public let displayName: String
	public let url: URL
	public let avatar: String

	public var instanceHost: String? {
		self.url.host
	}

	public enum CodingKeys: String, CodingKey {
		case username
		case displayName = "display_name"
		case url
		case avatar
	}
}
