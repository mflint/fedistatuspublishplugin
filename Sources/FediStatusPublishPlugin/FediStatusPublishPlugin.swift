import Publish
import Ink
import Files
import Foundation

public extension Plugin {
	static func fediStatus(renderer: StatusRenderer = DefaultStatusRenderer(),
						   caching: Bool = false) -> Self {
		if !caching {
			fputs("⚠️ FediStatus caching is disabled. Enable caching to improve generation speed\n", stdout)
		}

		return Plugin(name: "FediStatus") { context in
			context.markdownParser.addModifier(
				.statusBlockQuote(using: renderer,
								  context: context,
								  fileCaching: caching)
			)
		}
	}
}

private struct Cache: Codable {
	let status: Status
}

private extension Modifier {
	private static let prefix = "> fedistatus"

	static func statusBlockQuote<Site: Website>(using renderer: StatusRenderer,
												 context: PublishingContext<Site>,
												 fileCaching: Bool) -> Self {
		return Modifier(target: .blockquotes) { html, markdown in
			let markdown = String(markdown)
			guard markdown.starts(with: Self.prefix) else {
				return html
			}

			let scanner = Scanner(string: markdown)
			_ = scanner.scanString(Self.prefix)

			guard let statusURLString = scanner.scanUpToCharacters(from: .whitespacesAndNewlines) else {
				fatalError("Could not find status url in '\(markdown)'")
			}

			var cacheFile: File? = nil

			if let theCacheFile = try? context.cacheFile(named: statusURLString) {
				if fileCaching {
					// try to get the contents of the file cache
					cacheFile = theCacheFile
					if let oldCache = try? theCacheFile.read().decoded() as Cache {
						return try! renderer.render(status: oldCache.status)
					}
				} else {
					// delete the file cache
					try? theCacheFile.delete()
				}
			}

			let generator = StatusEmbedGenerator()
			let statusResult = generator.getStatus(statusUrlString: statusURLString)

			let status: Status
			switch statusResult {
			case .success(let theStatus):
				status = theStatus
			case .failure(let error):
				fatalError(String(describing: error))
			}

			// put in the file cache, if enabled
			if fileCaching,
			   let cacheFile = cacheFile {
				let newCache = Cache(status: status)
				try? cacheFile.write(newCache.encoded())
			}

			return try! renderer.render(status: status)
		}
	}
}
