# FediStatusPublishPlugin

## What is this?

This is a plugin for John Sundell's [Publish][publish-github] static site generator, which embeds a status from the fediverse (Mastodon, etc) in a generated article.

The plugin is inspired by [this post by Adam Cooper][real-men-wear-dresses] who did a similar thing for Hugo.


## How do I use it?

In `main.swift`:

```
import FediStatusPublishPlugin

try MyWebsite()
	.publish(using: [
				.installPlugin(.fediStatus())
	])
```

This will use a default `Renderer`. You can make your own renderer by passing it into the `fediStatus(` function:

```
.installPlugin(.fediStatus(renderer: FediverseStatusRenderer()))
```


### Including a status from the fediverse

In your `page.md` page, add a blockquote starting with the word `fedistatus`:

```
> fedistatus https://mastodon.me.uk/@matthew/109537487581976993
```

The parameter is the URL for the status.


## Styling the status

The default renderer contains some css classes, but if they're not to your liking then make your own renderer.

Then you'll need some CSS to make it look pretty. Here's an [example CSS file][example-css].


## Caching status content

You can also enable file caching, which caches status contents from one site-generation to the next - this makes generation faster, and gives a little protection against statuses being deleted:

```
.installPlugin(.fediStatus(renderer: FediverseStatusRenderer(), caching: true)),
```


## Feedback

Merge requests most welcome.


## Author

Matthew Flint, m@tthew.org


## License

_FediStatusPublishPlugin_ is available under the MIT license. See the LICENSE file for more info.


[publish-github]: https://github.com/johnsundell/publish
[real-men-wear-dresses]: https://realmenweardress.es/2022/11/2022-11-30-s3-hugo-toot-embeds/
[example-css]: https://gitlab.com/mflint/fedistatuspublishplugin/-/snippets/2475839
